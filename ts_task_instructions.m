function ts_task_instructions

global params
global exp

if strcmp(exp.data.language, 'English')
    m01 = 'Here are the instructions for your first task.';
    m02 = 'Note that there will be a mini-quiz at the end of the instructions to confirm your understanding,';
    m03 = 'so be sure to pay attention!';
    m11 = ' ';
    m12 = 'Your goal is to collect as many of these coins as possible:';
    m13 = ' ';
    m21 = 'At the beginning of each round, you will see two pictures:';
    m22 = 'You can select one of them using the left and right arrow keys.';
    m23 = 'Throughout the experiment, the pictures of this first stage will only be presented on the first stage.';
    m31 = 'Your chosen picture will appear';
    m32 = 'at the top of the next screen.';
    m33 = 'Two new pictures';
    m34 = 'will appear on bottom.';
    m35 = 'Each picture on the first screen';
    m36 = 'USUALLY leads to one specific pair';
    m37 = 'of pictures on this second screen.';
    m372 = 'So, the picture on top USUALLY';
    m373 = 'leads to the pictures on bottom.';
    m38 = 'HOWEVER, in SOME cases,';
    m39 = 'each picture leads to the';
    m310 = 'opposite pair of pictures.';
    m311 = 'So, the picture on top SOMETIMES leads to';
    m312 = 'different pictures than the ones on bottom.';
    m313 = 'Again, you will select one picture.';
    m314 = 'In this example, we will select the picture on the right.';
    m41 = 'After your second choice, you will either earn a reward (coin) or not (cross).';
    m411 = 'In this example, selecting the picture on the right led to a reward.';
    m42 = 'Some pictures of the second stage are more likely to lead to a reward than others.';
    m43 = 'The reward probability of each picture will CHANGE OVER TIME!';
    m44 = 'Therefore, your task is to determine which picture gives you the best chance of reward over time.';
    m45 = 'This means that it is impossible to win a coin on EVERY SINGLE trial.';
    m46 = 'Your goal is to win as many coins as possible by making good choices!';
    m51 = 'Do you have questions about this task?';
    m52 = 'We will now be moving on to a brief simulation of the task so you can try it yourself!';
    m53 = 'Remember, you will select the pictures using the left and right arrow keys.';
    m61 = 'The task structure can be depicted like this:';
    m62 = 'We will explain the image in further detail on the next screen;';
    m71 = 'When the right picture was chosen in stage 1 (square),';
    m72 = 'USUALLY the right picture pair appears in stage 2 (cross and heart);';
    m73 = 'But in some cases, the left picture pair might appear (circle and triangle).';
    m74 = 'The same is true for the left picture of stage 1 (pentagon):';
    m75 = 'In stage 2, it USUALLY leads to the left picture pair (circle and triangle),';
    m76 = 'but in SOME cases, it might lead to the right picture pair (cross and heart).';
    m81 = 'The picture pairs of the second stage are never separated.';
    m82 = 'That means that circle and triangle always appear together - the same is true for cross and heart.';
    m83 = 'And the same is true for the picture pair of the first stage.';
    m91 = 'The picture chosen in the second stage determines if you will get a reward or not.';
    m92 = 'The reward probability of each picture changes over time!';
    m101 = 'Please click the right arrow key.';
    m102 = 'You will see the stimulus you clicked.';
    m103 = 'In the actual task, the next pair will appear automatically.' ;
    m104 = 'For now, press any key to continue.';
    m105 = 'Congratulations! You got a reward.';
    m106 = 'We will now be moving on to the quiz.';
else
    m11 = ' ';
    m12 = 'In der folgenden Aufgabe ist dein Ziel so viele von diesen M�nzen wie m�glich zu finden:';
    m13 = ' ';
    m21 = 'Am Anfang jeder Runde wirst du zwei Bilder sehen. Dies ist die erste Stufe der Aufgabe.';
    m22 = 'Du kannst eines davon mit der linken oder rechten Pfeiltaste ausw�hlen.';
    m23 = 'Die Bilder der ersten Stufe werden im ganzen Experiment nur in der ersten Stufe erscheinen.';
    m31 = 'Dein in Stufe 1 gew�hltes Bild wird';
    m32 = 'am n�chsten Bildschirm oben sein.';
    m33 = 'Zwei neue Bilder';
    m34 = 'werden darunter erscheinen.';
    m35 = 'Jedes Bild der ersten Stufe f�hrt';
    m36 = 'NORMALERWEISE zu einem bestimmten';
    m37 = 'Bildpaar in der zweiten Stufe.';
    m372 = 'Das Bild oben f�hrt also';
    m373 = 'NORMALERWEISE zu den Bildern unten.';
    m38 = 'ABER MANCHMAL f�hrt';
    m39 = 'jedes Bild zu dem';
    m310 = 'entgegengesetzten Bildpaar.';
    m311 = 'Das Bild oben f�hrt also MANCHMAL';
    m312 = 'zu anderen Bildern als denen hier.';
    m313 = 'Du wirst wieder ein Bild ausw�hlen.';
    m41 = 'F�r das gew�hlte Bild wirst du entweder belohnt (M�nze) oder nicht (Kreuz).';
    m42 = 'Einige Bilder der zweiten Stufe werden wahrscheinlicher belohnt als andere.';
    m43 = 'Die Belohnungswahrscheinlichkeit jedes Bildes wird sich aber MIT DER ZEIT VER�NDERN!';
    m44 = 'Daher musst du st�ndig ausprobieren, welches Bild wann die gr��te Belohnungswahrscheinlichkeit hat.';
    m45 = 'Das bedeutet, dass es unmoeglich ist jedes Mal zu gewinnen;';
    m46 = 'also triff die richtigen Entscheidungen, um so viele Muenzen wie moeglich zu gewinnen!';
    m51 = 'Hast noch Fragen zur Aufgabe?';
    m52 = 'Dr�cke eine beliebige Taste, um die Aufgabe zu starten!';
    m61 = 'Die Aufgabe l�sst sich also auch folgenderma�en darstellen:';
    m71 = 'Wenn in Stufe 1 das rechte Bild gew�hlt wurde (Quadrat),';
    m72 = 'erscheint in Stufe 2 NORMALERWEISE das rechte Bildpaar (Kreuz und Herz);';
    m73 = 'Aber in MANCHEN F�llen erscheint auch das linke Bildpaar (Kreis und Dreieck).';
    m74 = 'Dasselbe gilt f�r das linke Bild der ersten Stufe (F�nfeck):';
    m75 = 'NORMALERWEISE f�hrt es zum linken Bildpaar der zweiten Stufe (Kreis und Dreieck),';
    m76 = 'doch MANCHMAL f�hrt es auch zum rechten Bildpar (Kreuz und Herz).';
    m81 = 'Die Bildpaare der zweiten Stufe werden niemals getrennt.';
    m82 = 'Kreis und Dreieck bleiben also immer zusammen - genauso wie Kreuz und Herz.';
    m83 = 'Das gleiche gilt auch f�r das Bildpaar der ersten Stufe.';
    m91 = 'Das Bild, das in der zweiten Stufen gew�hlt wurde, bestimmt, ob man eine Belohung bekommt oder nicht.';
    m92 = 'Die Belohnungswahrscheinlichkeit von jedem Bild ver�ndert sich aber mit der Zeit!';
end

cgfont(params.text.font, params.text.font_size);
cgpencol(params.text.color);

%% 0th slide - quiz notification
upper_end = 200;
cgmakesprite(695, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(695);

cgtext(m01, 0, upper_end+100);
cgtext(m02, 0, upper_end);
cgtext(m03, 0, upper_end-100);

ts_wait_input(695, 0, 0);

%% 1st slide
cgmakesprite(700, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(700);

cgtext(m11, 0, upper_end);
cgtext(m12, 0, upper_end-100);
cgtext(m13, 0, upper_end-120);
cgloadbmp(701, 'stimuli/r1.bmp', 200, 200);
cgdrawsprite(701, 0, upper_end-300);

ts_wait_input(700, 0, 0);

%% 2nd slide
cgmakesprite(705, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(705);

cgtext(m21, 0, upper_end);
cgtext(m22, 0, -upper_end);
cgloadbmp(706, 'stimuli/1choice.bmp', params.resolution(1), 0);
cgdrawsprite(706, 0, 0);

ts_wait_input(705, 0, 0);

%% 3rd slide
cgmakesprite(710, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(710);

cgloadbmp(711, 'stimuli/3nextchoice.bmp', params.resolution(1), 0);
cgdrawsprite(711, 0, 0);
cgtext(m31, 0, 20);
cgtext(m32, 0, 0);
cgtext(m33, 0, -30);
cgtext(m34, 0, -50);
cgtext(m35, 0, -80);
cgtext(m36, 0, -100);
cgtext(m37, 0, -120);
cgtext(m372, 0, -140);
cgtext(m373, 0, -160);
cgtext(m38, 0, -190);
cgtext(m39, 0, -210);
cgtext(m310, 0, -230);
cgtext(m311, 0, -250);
cgtext(m312, 0, -270);
ts_wait_input(710, 0, 0);

cgsetsprite(711);
cgdrawsprite(711, 0, 0);
cgtext(m313, 0, -300);
cgtext(m314, 0, -320);
ts_wait_input(711, 0, 0);

%% 4th slide
cgmakesprite(715, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(715);

cgloadbmp(716, 'stimuli/4reward.bmp', params.resolution(1), 0);
cgdrawsprite(716, 0, 0);
cgtext(m41, 0, upper_end);
cgtext(m411, 0, upper_end-20);
ts_wait_input(715, 0, 0);

cgsetsprite(716);
cgtext(m42, 0, upper_end-40);
cgtext(m43, 0, upper_end-60);
cgtext(m44, 0, upper_end-80);
cgtext(m45, 0, upper_end-120);
cgtext(m46, 0, upper_end-140);
ts_wait_input(716, 0, 0);

%% Next slide
cgmakesprite(735, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(735);

cgloadbmp(736, 'stimuli/E1.bmp');
cgdrawsprite(736, 0, 0);
cgtext(m61, 0, 350);
cgtext(m62, 0, 330);

ts_wait_input(735, 0, 0);

%% Next slide
cgmakesprite(740, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(740);
cgloadbmp(741, 'stimuli/E2.bmp');
cgdrawsprite(741, 0, 0);
cgtext(m71, 0, 350);
cgtext(m72, 0, 330);
cgtext(m73, 0, 310);
ts_wait_input(740, 0, 0);

cgsetsprite(740);
cgloadbmp(741, 'stimuli/E2.bmp');
cgdrawsprite(741, 0, 0);
cgtext(m74, 0, -310);
cgtext(m75, 0, -330);
cgtext(m76, 0, -350);
ts_wait_input(740, 0, 0);

%% Next slide
cgmakesprite(745, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(745);
cgloadbmp(746, 'stimuli/E3.bmp');
cgdrawsprite(746, 0, 0);
cgtext(m81, 0, 350);
cgtext(m82, 0, 330);
cgtext(m83, 0, 310);
ts_wait_input(745, 0, 0);

%% Next slide
cgmakesprite(750, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(750);
cgloadbmp(751, 'stimuli/E4.bmp');
cgdrawsprite(751, 0, 0);
cgtext(m91, 0, 350);
cgtext(m92, 0, 330);
ts_wait_input(750, 0, 0);

%% Last slide before transitioning to a demo
cgmakesprite(720, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(720);

cgtext(m51, 0, upper_end);
cgtext(m52, 0, -upper_end + 20);
cgtext(m53, 0, -upper_end);

ts_wait_input(720, 0, 0);

%% Demo 1st slide - beginning of demo
for i = 1:5             %params.task.test.number_of_trials
    
    params.user.log(i,1) = i;
    
    %% show ITI
    iti = ts_iti(params.task.iti);
    params.user.log(i,9) = iti;
    
    %% show stage 1
    ts_single_trial_first_stage(i);    %the function logs stimulus presentation and response
    
    %% show feedback for 1st choice
    ts_feedback_first_stage(i);
    
    if params.user.log(i,2) ~= 0       % if the first-stage key is not empty
        %% determine the transition
        ts_determine_transition(i);

        %% show stage 2
        ts_single_trial_second_stage(i);

        %% determine and show feedback
        ts_feedback_second_stage(i);
    end
end

%% Last slide before transitioning to the quiz
cgfont(params.text.font, params.text.font_size);
cgpencol(params.text.color);

cgmakesprite(775, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(775);

cgtext(m106, 0, upper_end);
cgtext(m51, 0, -upper_end);

ts_wait_input(775, 0, 0);
