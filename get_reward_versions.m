file_dir = 'C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\Results\rawdata\';
%file_dir = 'C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\Results\rawdata\';

files = dir(file_dir);
fileIndex = find(~[files.isdir]);

version_dat = zeros(64, 3);

for fili = 1:length(fileIndex)
    
    fileName = files(fileIndex(fili)).name;
    load([file_dir fileName]);
    
    version_dat(fili, 1) = str2double(params.user.number);
    version_dat(fili, 2) = params.user.session;
    version_dat(fili, 3) = params.user.version;
    
end

csvwrite([file_dir 'reward_versions.csv'], version_dat);