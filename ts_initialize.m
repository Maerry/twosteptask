
function ts_initialize

global params;
global exp;

% check if current working directory is the right one
temp_wd = pwd;
if strcmp(temp_wd(end-10:end), 'TwoStepTask') ~= 1
   error('please change working directory to \TwoStepTask')
end

%% trial parameters
% Approximate durations: .7s first RT + 0.5s transition + 1s second RT + 
% 0.8s reward + 1s iti = 4s/trial
% Practice Duration =  50 trials = 3min18sec
% Test Duration     = 180 trials = 12min
params.trial.response_deadline = 10000; %2000;
params.trial.reward_duration = 800; %2000;
params.trial.transition_duration = 500; %1500;

params.background = [0 0 0];
params.stimulus.box_size = 300;
params.stimulus.char_size = 230;
params.stimulus.reward_size = 150;

params.stimulus.colors = [1 0 0; 0 1 0; 1 0 1; 0 1 1];
params.stimulus.colors = params.stimulus.colors(randperm(4),:); %. Color of second phase stimuli.after practice phase, first two rows are chopped off. This way, same scripts can be used (single_trial.*. etc)
params.stimulus.colors_prac = [];
params.stimulus.first_phase_color = [0.3 0.3 0.3];
params.stimuli_prac = [];
params.stimulus.location = 300;
params.stimulus.center_y = 200;
params.stimulus.side_y = -130;
params.stimulus.move_speed = 25;

params.user.log = []; %holding place for logfiles. See end of this file for overview of log columns
params.user.prac_cash = 0; %how much cash the user has saved up, in points
params.user.exp_cash = 0; %in points
params.user.colours = []; %here colour scheme for this user will be held

params.task.probabilities = [];
params.task.probabilities_prac = [];
params.task.lower_boundary = 0; % nathaniel used 0.25
params.task.upper_boundary = 1; % nathaniel used 0.75
params.task.common_transition_prob = 0.7; %nathaniel used 0.7
params.task.iti = [0.8 1.5];%[1 3];
params.task.conversion_rate = 1/4;

params.task.break.frequency = 95;%68; %after this number of trials, give a break. Nathaniel had 67 (in scanner...)
params.task.break.duration = 30;

params.text.font_size = 20;
params.text.font = 'Helvetica';
params.text.color = [1 1 1];

params.resolution = [1024 768];
params.display_type = 1; %1 fullscreen, 0 windowed

params.keys.left = 75;
params.keys.right = 77;

%% Load reward probabilities & fractals (depending on exp.version)
params.stimuli = [];
if exp.version == 'A'
    load stimuli/tryout_list1
    for i = 1:6
        params.stimuli{i} = ['f' num2str(i) '.bmp'];
    end
elseif exp.version == 'B'
    load stimuli/tryout_list2
    for i = 1:6
        j = i + 6;
        params.stimuli{i} = ['f' num2str(j) '.bmp'];
    end
elseif exp.version == 'C'
    load stimuli/tryout_list3
    for i = 1:6
        j = i + 12;
        params.stimuli{i} = ['f' num2str(j) '.bmp'];
    end
elseif exp.version == 'D'
    load stimuli/tryout_list4
    for i = 1:6
        j = i + 18;
        params.stimuli{i} = ['f' num2str(j) '.bmp'];
    end
end
params.task.probabilities = x;
%%% Randomize fractal order
params.stimuli = params.stimuli(randperm(6));
params.stimuli = char(params.stimuli);

%% Get number of trials & stimulus colors & practice fractals (depending on practice or test)
number_of_trials          = exp.numb_of_trials.two_step;
params.stimulus.colors    = params.stimulus.colors(3:4,:);

%% Performance Summary Slides
exp.feedback_trials = zeros(1, number_of_trials);
exp.summary_feedback_trial.two_step = ceil(number_of_trials / exp.number_of_summary_feedbacks);
exp.feedback_trials(mod(1:number_of_trials, exp.summary_feedback_trial.two_step) == 0) = 5;
exp.feedback_trials(number_of_trials) = 5;
