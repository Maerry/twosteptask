
function ts_initialize

global params;
global exp;

% check if current working directory is the right one
temp_wd = pwd;
if strcmp(temp_wd(end-10:end), 'TwoStepTask') ~= 1
   error('please change working directory to \TwoStepTask')
end

%% trial parameters
params.background = [0 0 0];

% Approximate duration: .7s first RT + 0.8s transition + 1s second RT + 1s
% reward + 1s iti = 4.5s/trial * (51 prac + 201 test) = 19min!
params.trial.response_deadline = 10000;%2000;
params.trial.reward_duration = 1000;%2000;
params.trial.transition_duration = 700;%1500;

params.stimulus.box_size = 300;
params.stimulus.char_size = 230;
params.stimulus.reward_size = 150;

params.stimulus.colors = [1 0 0;0 1 0;1 0 1;0 1 1];
ix = randperm(4);
params.stimulus.colors = params.stimulus.colors(ix,:); %. Color of second phase stimuli.after practice phase, first two rows are chopped off. This way, same scripts can be used (single_trial.*. etc)
params.stimulus.colors_prac = [];
params.stimulus.first_phase_color = [0.3 0.3 0.3];
params.stimuli_prac = [];
params.stimulus.location = 300;
params.stimulus.center_y = 200;
params.stimulus.side_y = -130;
params.stimulus.move_speed = 25;

% params.user.initials = input('Subject initials: ','s'); %just a control to make sure subject number is correct, can be compared to subject list
% params.user.number = str2num(input('Subject number: ','s')); %this will be used for logging
% params.user.session = str2num(input('Session number: ','s'));
% params.user.date = str2num(input('date (yyyymmdd): ','s'));

%params.user.initials    = exp.data.initials;
%params.user.number      = exp.subj;
%params.user.session     = exp.session;
%params.user.date        = exp.data.date1;
%order = [mod(params.user.number, 2) + 1 2 - mod(params.user.number, 2)];
%params.user.version = order(params.user.session);

params.user.log = []; %holding place for logfiles. See end of this file for overview of log columns
params.user.prac_cash = 0; %how much cash the user has saved up, in points
params.user.exp_cash = 0; %in points
params.user.colours = []; %here colour scheme for this user will be held

params.task.probabilities = [];
params.task.probabilities_prac = [];
params.task.lower_boundary = 0; % nathaniel used 0.25
params.task.upper_boundary = 1; % nathaniel used 0.75
params.task.common_transition_prob = 0.7; %nathaniel used 0.7
params.task.prac.number_of_trials = 50; %nathaniel had 50 (outside scanner)
params.task.test.number_of_trials = 201; %nathaniel had 201 (in scanner)
params.task.iti = [0.8 1.5];%[1 3];
params.task.conversion_rate = 1/4;

params.task.break.frequency = 101;%68; %after this number of trials, give a break. Nathaniel had 67 (in scanner...)
params.task.break.duration = 30;

params.text.font_size = 20;
params.text.font = 'Helvetica';
params.text.color = [1 1 1];

params.resolution = [1024 768];
params.display_type = 1; %1 fullscreen, 0 windowed

params.keys.left = 75;
params.keys.right = 77;

%% Stimulus randomization
params.stimuli = [];
if exp.session == 1
    for i = 1:12
        params.stimuli{i} = ['f' num2str(i) '.bmp'];
    end
else
    for i = 1:12
        j = i + 12;
        params.stimuli{i} = ['f' num2str(j) '.bmp'];
    end
end
ix = randperm(12);
params.stimuli = params.stimuli(ix);
params.stimuli = char(params.stimuli);

%% Load the random walks of the probabilities
if params.user.version == 1
    load('stimuli\list1.mat')
elseif params.user.version == 2
    load('stimuli\list2.mat')
end
params.task.probabilities = x;
