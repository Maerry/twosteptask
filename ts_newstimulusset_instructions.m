function ts_newstimulusset_instructions

global params
global exp

if strcmp(exp.data.language, 'English')
    m1 = 'Your task will now be to find as many coins as possible, just like before.';
    m2 = 'The pictures and reward probabilities will be different than before, but the rules of the game are exactly the same.';
    m3 = 'Press any key when you are ready to start.';
else
    m1 = 'Du musst nun wieder versuchen m�glichst viele M�nzen zu sammeln, genau wie vorher.';
    m2 = 'Die Bilder und Belohnungswahrscheinlichkeiten werden anders sein als voher, die Spielregeln sind aber genau die selben.';
    m3 = 'Dr�cke eine beliebige Taste, wenn du bereit bist.';
end

cgfont(params.text.font, params.text.font_size);
cgpencol(params.text.color);

upper_end = 200;
cgmakesprite(725, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(725);

cgtext(m1, 0, upper_end);
cgtext(m2, 0, upper_end-20);
cgloadbmp(726, 'stimuli/r1.bmp', 200, 200);
cgdrawsprite(726, 0, 0);
cgtext(m3, 0, -upper_end);

ts_wait_input(725, 0, 0);
