function ts_start

global params;

%% start cogent
cgloadlib;
try
    cgopen(params.resolution(1), params.resolution(2), 32, 0, params.display_type);
catch
    input('Please press enter to continue')
    cgopen(params.resolution(1), params.resolution(2), 32, 0, params.display_type);
end

%% load reward sprite
cgloadbmp(401, 'stimuli/r1.bmp', params.stimulus.reward_size, params.stimulus.reward_size);
