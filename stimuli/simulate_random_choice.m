%%
load list1
for b=1:10000
    prob = x(5:8,:);
    choice = randi(4,1,200);
    for k=1:4
        c(k,:) = choice==k;
    end
    rew(b) = sum(rand(200,1) < prob(c));
end

load list2
for b=10001:20000
    prob = x(5:8,:);
    choice = randi(4,1,200);
    for k=1:4
        c(k,:) = choice==k;
    end
    rew(b) = sum(rand(200,1) < prob(c));
end
