function ts_close

global params
global exp

%% Save data
result_file_name = [exp.results_filepath, sprintf('Results/TS_%03s_%i%s.mat', exp.subj, exp.session, exp.version)];
save(result_file_name, 'params');

%% Prepare and present message  
%%% Calculate task bonus
received_points = nansum(params.user.log(:,10));
min_points      = sum(min(params.task.probabilities(:,1:exp.numb_of_trials.two_step)));   % If subject always choses the worst option
if received_points < min_points
    min_points = received_points;                        
end
max_points      = sum(max(params.task.probabilities(:,1:exp.numb_of_trials.two_step)));   % If subject always choses the best option

bonus = (received_points - min_points) / (max_points - min_points) * ...
    exp.bonus.max_session/6;

%%% Store task bonus (to calculate total bonus at the end)
if any(strcmp(exp.version, {'A', 'C'}))
    exp.bonus.two_step1 = round(bonus, 2);
elseif any(strcmp(exp.version, {'B', 'D'}))
    exp.bonus.two_step2 = round(bonus, 2);
end

m11 = 'Great job!';
m12 = ['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
m13 = '(This bonus is based on your performance and added';
m14 = 'to your regular payment.)';
if any(strcmp(exp.version, {'A', 'C'}))
    m15 = 'Press space when you want to move on to your next task.';
elseif any(strcmp(exp.version, {'B', 'D'}))
    m15 = 'This was your last task for today! Thank you!';
end
cgmakesprite(500, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(500);
cgtext(m11, 0, 290);
cgtext(m12, 0, 200);
cgtext(m13, 0, 0);
cgtext(m14, 0, -50);
ts_wait_input(500, 0, 0);

cgsetsprite(500);
cgtext(m15, 0, -250);
ts_wait_input(500, 0, 0);

%%% Shut cogent (within loop because not after training version)
cgshut

params.user.log = [];  %after training, clean the data log
