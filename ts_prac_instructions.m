function ts_prac_instructions

global params;

cgfont(params.text.font, params.text.font_size);
cgpencol(params.text.color);

msg1 = 'Welcome!';
msg2 = 'You will start with some practice rounds to get used to the task';
msg3 = 'Feel free to ask the experimenter if anything is unclear at any point';
msg4 = 'You will not earn money during the practice phase!';
msg5 = 'Press any key to continue';

cgmakesprite(102,params.resolution(1), params.resolution(2), params.background);
cgsetsprite(102);
% cgtext(msg1,0,params.resolution(1)/6);
cgtext(msg2,0,0);
cgtext(msg3,0,-params.resolution(1)/12);
cgtext(msg4,0,-params.resolution(1)/10);
cgtext(msg5,0,-params.resolution(1)/8);

ts_wait_input(102,0,0);