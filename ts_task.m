function ts_task

%% Columns in the log file
% 
% 1. trial number
% 2. first stage keypress (0 means too late)
% 3. second stage keypress
% 4. first stage stimulus chosen
% 5. second stage stimulus chosen
% 6. second stage pair shown
% 7. rt first stage (NaN means too late, 0 means they had the key pressed down)
% 8. rt second stage
% 9. ITI going into this trial
% 10. reward received
% 11. 1st phase, stimulus left
% 12. 1st phase, stimulus right
% 13. 2nd phase, stimulus left
% 14. 2nd phase, stimulus right
% 15. common (0) or uncommon (1) transition

global params
global exp

%% Present the task
for trial = 1:exp.numb_of_trials.two_step             %params.task.test.number_of_trials
    
    params.user.log(trial,1) = trial;
    
    %% show ITI
    iti = ts_iti(params.task.iti);
    params.user.log(trial,9) = iti;
    
    %% show stage 1
    ts_single_trial_first_stage(trial);    %the function logs stimulus presentation and response
    
    %% show feedback for 1st choice
    ts_feedback_first_stage(trial);
    
    if params.user.log(trial,2) ~= 0                                        % if the first-stage key is not empty
        %% determine the transition
        ts_determine_transition(trial);

        %% show stage 2
        ts_single_trial_second_stage(trial);

        %% determine and show feedback
        ts_feedback_second_stage(trial);

    else                                                                    % if the first-stage key IS empty
        params.user.log(trial,[3 4 5 6 7 8 10 13 14 15]) = NaN;
    end
     
    %% Insert summary feedback
    if exp.feedback_trials(trial) == 5
        pr_summary_feedback(trial);
    end
    
end