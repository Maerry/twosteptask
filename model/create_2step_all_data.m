%% Save 2-step data as .csv (filename: 2step_all_data)

% Set directions to folders containing experiment data (file_dir) and scripts (script_dir)
file_dir = 'C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\Results';
script_dir = 'C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\model';
% file_dir = 'C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\Results';
% script_dir = 'C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\model';

cd(script_dir)
addpath(genpath(script_dir))

data_import = cell(32,1);

files = dir([file_dir '\rawdata\']);
fileIndex = find(~[files.isdir]);

for k = 1:length(fileIndex)
    % % find and load each file in the directory % %
    fileName = files(fileIndex(k)).name;
    load([file_dir '\rawdata\' fileName]);                                  % load each data file in the folder
    trial = params.user.log;                                                % save data file into 'trial'
    
    % % get run (1 or 2) % %
    if length(trial) == 50                                                  % find out if the data are from training (0) or testing run (1)
        run = 1;
    else
        run = 2;
    end
    
    % % get switch/stay columns % %
%     [stim1, key1, rep_stim1, rep_key1] = swstay_M(trial);                   % the 2 columns correspond to the 2 stimuli of the 1st stage; stay indicates if the 1st stimulus was repeatedly chosen (column 1) or the 2nd one (column 2) or either (column 3)
    
    % % exclude invalid trials (no response) % %
    valid = trial(:,2) ~= 0 & trial(:,3) ~= 0;                              % find all trials where responses were given in 1st AND 2nd stage
    trial = trial(valid,:);                                                 % exclude all other trials
    
    % % get SubjID and session % %
    SubjID = str2double(params.user.number) * ones(length(trial),1);        % save SubjID
    session = params.user.session * ones(length(trial),1);                  % save session (1 or 2)
    runs = run * ones(length(trial),1);                                     % make run the right length
    
    % % save everything into data_import % %
%     data_import{k,1} = [trial, stim1, rep_stim1, key1, rep_key1, SubjID, session, runs];
    data_import{k,1} = [trial, SubjID, session, runs];
    
    two_step_subjdata = cell2mat(data_import(k,1));                               % all log files
    csvwrite([file_dir '\csvs\2step_all_data' num2str(k) '.csv'], two_step_subjdata);
    
end

two_step_alldata = cell2mat(data_import(:,1));                               % all log files
csvwrite([file_dir '\all\2step_all_data.csv'], two_step_alldata);
