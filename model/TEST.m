clc
clear

par = [.5 .5  .5 .5  .5 .5 .5  .5];

% Set directions to folders containing experiment data (file_dir) and scripts (script_dir)
file_dir = 'C:\Dropbox\Sylvain_Maria\';
script_dir = 'C:\Dropbox\Sylvain_Maria\';
cd(script_dir)
addpath(genpath(script_dir))

files = dir([file_dir '\rawdata\']);
fileIndex = find(~[files.isdir]);

% % find and load each file in the directory % %
fileName = files(fileIndex(1)).name;
load([file_dir '\rawdata\' fileName]);                                  % load each data file in the folder
trial = params.user.log; 

[ reward, choice1, choice2, key1, key2, k1, valid, ch1 ] = GetTrialData( trial ) ;

tic
for i=1:100 ;
    
    [lik, prob, CR, VAL, BIC] = get_log_likelihood_of_params(par, reward, choice1, choice2, key1, key2, k1, valid, ch1) ;

end
toc
