function [ reward, choice1, choice2, key1, key2, k1, valid, ch1 ] = GetTrialData( trial )

    %% extract data from log file (missed, reward, fractal choices, key presses)
    missed = zeros(size(trial, 1),1);
    reward = trial(:,10);
    % Create new vectors for fractal choices and key presses that have one row for each possible decision and are 1 whenever this decision was made
    ch1 = trial(:,4);
    choice1 = zeros(length(ch1),2);
    for i=1:length(ch1)
        try choice1(i,ch1(i)) = 1;
        catch
            missed(i) = 1;
        end
    end
    ch2 = trial(:,5);
    choice2 = zeros(length(ch2),6);
    for i=1:length(ch2)
        try choice2(i,ch2(i)) = 1;
        catch
            missed(i) = 2;
        end
    end
    k1 = trial(:,2);
    key1 = zeros(length(k1), 2);
    for i = 1:length(k1)
        try key1(i, k1(i)) = 1;
        catch
            missed(i) = 1;
        end
    end
    k2 = trial(:,3);
    key2 = zeros(length(k2), 2);
    for i = 1:length(k2)
        try key2(i, k2(i)) = 1;
        catch
            missed(i) = 2;
        end
    end
    % Remove invalid (= missed) trials
    valid = find(missed == 0);
    reward = reward(valid);
    choice1 = choice1(valid,:);
    ch1 = ch1(valid);
    choice2 = choice2(valid,:);
    ch2 = ch2(valid);
    key1 = key1(valid,:);
    k1 = k1(valid);
    key2 = key2(valid,:);
    k2 = k2(valid);

end
