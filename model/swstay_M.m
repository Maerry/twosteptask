function [choice1, key1, rep_choice1, rep_key1] = swstay_M(trial)
% function [lik, prob] = lik2step(trial, par)
% Parameter fitting for 2step task
% kwunder@fil.ion.ucl.ac.uk

%% Columns in the log file
% 
% 1. trial number
% 2. first stage keypress
% 3. second stage keypress
% 4. first stage stimulus chosen
% 5. second stage stimulus chosen
% 6. second stage pair shown
% 7. rt first stage
% 8. rt second stage
% 9. ITI going into this trial
% 10. reward received
% 11. 1st phase, stimulus left
% 12. 1st phase, stimulus right
% 13. 2nd phase, stimulus left
% 14. 2nd phase, stimulus right
% 15. common (0) or uncommon (1) transition

missed = zeros(length(trial),1);                                            % initialize: no trial is missed

% % Create stim_chosen1 vectors (first column is 1 every time the first stimulus is chosen; second column is 1 every time the second stimulus is chosen)
stim_chosen1 = trial(:,4);                                                  % take stimulus choice at step1 from data frame
choice1 = zeros(length(stim_chosen1), 2);                                   % initialize choice at step1 to zeros, with 2 columns corresponding to the 2 possible choices
for i = 1:length(stim_chosen1)
    try choice1(i, stim_chosen1(i)) = 1;                                    % the column corresponding to the choice will be 1 for each choice
    catch
        missed(i) = 1;                                                      % and will not be filled if no choice was made (and remembered in var missed)
    end
end

% % Create key_chosen1 vectors (first column is 1 every time the first key is chosen; second column is 1 every time the second key is chosen)
key_chosen1 = trial(:,2);                                                   % take key press at step1 from data frame
key1 = zeros(length(key_chosen1), 2);
for i = 1:length(key_chosen1)
    try key1(i, key_chosen1(i)) = 1;
    catch
%         missed(i) = 1;
    end
end

% % Create stim_chosen2 vectors (first row is 1 every time the first stimulus is chosen; second row is 1 every time the second stimulus is chosen)
stim_chosen2 = trial(:,5);                                                  % Same for choice of the second stage (but with more columns for more pictures)
choice2 = zeros(length(stim_chosen2), 6);
for i = 1:length(stim_chosen2)
    try choice2(i, stim_chosen2(i)) = 1;
    catch
        missed(i) = 2;                                                      % This is the important part!! Find trials where the second stage was unanswered
    end
end

%% Remove all invalid = non-response rows from all data frames
% choice2 = [1 0 0 0]; choice1 = [1 0];
% reward = reward(valid);
choice1 = choice1(missed == 0,:);
key1 = key1(missed == 0,:);
% stim_chosen1 = stim_chosen1(valid);
% choice2 = choice2(valid,:);
% ch2 = ch2(valid);
% choice = [choice1 choice2(:,3:6)];


%% repetition perseveration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Stimulus repetition
diff_choice1     = [1 1; diff(choice1)];                                    % all 0s ('this row does not differ from the next row') will be 1s in rep_choice; diff() calculates difference between one row and the previous row; it sets the first row to 0; I add 1s in the very first row because there was NO stay
rep_choice1      = (diff_choice1 == 0) .* (choice1 == 1);                   % fill the row corresponding to the chosen picture (choice1 == 1) with 1's, whenever no change in choice occured (dchoice == 0)
rep_choice1(:,3) = sum(rep_choice1, 2);                                     % third columns indactes whenever a choice was repeated for any picture

% % Key repetition
diff_key1        = [1 1; diff(key1)];                                              % diff() calculates difference between one row and the previous row; therefore, the first row tells us if there was a change from the 1st to the 2nd trial, i.e., if the subjects switched IN THE 2ND TRIAL; that is exactly where we need it if we want to compare reward in trial i with switchinb behavior in the following trial i+1; therefore, I will attach the [0 0] row on the end of the matrix (the reward of the very last trial can never be followed by a switch because the task is over)
rep_key1         = (diff_key1 == 0) .* (key1 == 1);                                 % fill the row corresponding to the chosen picture (choice1 == 1) with 1's, whenever no change in key press occured (dchoice == 0)
rep_key1(:,3)    = sum(rep_key1, 2);                                           % third columns indactes whenever a choice was repeated for any picture

% NO NEED TO CHANGE ANYTHING IN CHOICE1; every row corresponds to the stimulus chosen in this particular trial, with these particular rewards

% % ORIGINAL CODE!!! % %
% dchoice = [0 0; diff(choice1)];                                             % first row = [0 0]; diff() calculates difference between one row and the previous
% rep = (dchoice == 0) .* (choice1 == 1);                                     % fill the row corresponding to the chosen picture (choice1 == 1) with 1's, whenever no change in choice occured (dchoice == 0)
% rep(:,3) = sum(rep, 2);                                                     % third columns indactes whenever a choice was repeated for any picture
% 
% C = [0 0; choice1(1:end-1,:)];                                              % 'move the whole vector down' by one trial (introduce an additional row in the beginning and delete last row)
