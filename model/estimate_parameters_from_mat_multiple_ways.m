
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% READ ME: How this code works %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% what_it_will_do is a matrix that specifies which models will be tested

%%% what_it_will_do has one ROW for each model:
%  1. full model (alpha1, alpha2, beta1, beta2, lambda, p, k, w)
%  2. one alpha  (alpha,          beta1, beta2, lambda, p, k, w)
%  3. one beta   (alpha1, alpha2, beta,         lambda, p, k, w)
%  4. one alpha & one beta
%  5. ... (see below)

%%% what_it_will_do has the following COLUMNS:
%  1.     num_params
%  2.- 9. lower_param_boundaries - (alpha1, alpha2, beta1, beta2, lambda, p, k, w)
% 10.-17. upper_param_boundaries - (alpha1, alpha2, beta1, beta2, lambda, p, k, w)
% 18.-25. param_start_values     - (alpha1, alpha2, beta1, beta2, lambda, p, k, w)


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Define the models that will be tested %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

param_start_values = [0 0 0 0 0 0 0 0];
solver = 'ga_and_fmincon';

full_model         = {8, [0 0 0 0 0 -5 -5 .01], [1 1 20 20 1 5 5 .99], param_start_values, 'aabblpkw'};
abblpkw            = {7, [0 0 0 0 0 -5 -5 .01], [1 0 20 20 1 5 5 .99], param_start_values, 'abblpkw'};
aablpkw            = {7, [0 0 0 0 0 -5 -5 .01], [1 1 20  0 1 5 5 .99], param_start_values, 'aablpkw'};
aabbpkw            = {7, [0 0 0 0 1 -5 -5 .01], [1 1 20 20 1 5 5 .99], param_start_values, 'aabbpkw'};
aabblkw            = {7, [0 0 0 0 0  0 -5 .01], [1 1 20 20 1 0 5 .99], param_start_values, 'aabblkw'};
aabblpw            = {7, [0 0 0 0 0 -5  0 .01], [1 1 20 20 1 5 0 .99], param_start_values, 'aabblpw'};
aabblpk            = {7, [0 0 0 0 0 -5 -5 .50], [1 1 20 20 1 5 5 .50], param_start_values, 'aabblpk'};

ablpkw             = {6, [0 0 0 0 0 -5 -5 .01], [1 0 20  0 1 5 5 .99], param_start_values, 'ablpkw'};
abpkw              = {5, [0 0 0 0 1 -5 -5 .01], [1 0 20  0 1 5 5 .99], param_start_values, 'abpkw'};   % used to be the best model
ablkw              = {5, [0 0 0 0 0  0 -5 .01], [1 0 20  0 1 0 5 .99], param_start_values, 'ablkw'};
ablpw              = {5, [0 0 0 0 0 -5  0 .01], [1 0 20  0 1 5 0 .99], param_start_values, 'ablpw'};
ablpk              = {5, [0 0 0 0 0 -5 -5 .50], [1 0 20  0 1 5 5 .50], param_start_values, 'ablpk'};

abkw               = {4, [0 0 0 0 1  0 -5 .01], [1 0 20  0 1 0 5 .99], param_start_values, 'abkw'};
abpw               = {4, [0 0 0 0 1 -5  0 .01], [1 0 20  0 1 5 0 .99], param_start_values, 'abpw'};
abpk               = {4, [0 0 0 0 1 -5 -5 .50], [1 0 20  0 1 5 5 .50], param_start_values, 'abpk'};

abk                = {3, [0 0 0 0 1  0 -5 .50], [1 0 20  0 1 0 5 .50], param_start_values, 'abk'};
abp                = {3, [0 0 0 0 1 -5  0 .50], [1 0 20  0 1 5 0 .50], param_start_values, 'abp'};

what_it_will_do = [full_model;
                   abblpkw; aablpkw; aabbpkw; aabblkw; aabblpw; aabblpk;
                   ablpkw; abpkw; ablkw; ablpw; ablpk;
                   abkw; abpw; abpk;
                   abk; abp];
                         

%% %%%%%%%%%%%%%%%%%%%%
%%% Test the models %%%
%%%%%%%%%%%%%%%%%%%%%%%

for row = 16:17   %size(what_it_will_do, 1):-1:1    %1:size(what_it_will_do, 1)
    estimate_parameters_from_mat(what_it_will_do{row, 1}, ...               % num_params
                                 what_it_will_do{row, 2}, ...               % lower_param_boundaries
                                 what_it_will_do{row, 3}, ...               % upper_param_boundaries
                                 what_it_will_do{row, 4}, ...               % param_start_values
                                 solver, ...                                % algorithm(s) used to find minima
                                 what_it_will_do{row, 5})                   % unique filename suffix
end

