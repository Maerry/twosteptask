function [lik, prob, CR, VAL, BIC] = get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left )

% Combine 1st- and 2nd-stage choices and 1st- and 2nd-stage key presses in a single matrix
choice = [choice1 choice2(:,3:6)];
key = [key1 key2];

%% parameters
% % If parameters should range between 0-inf % %
% alpha1 = 1/(1+exp(-par(1)));  % RL 1
% alpha2 = 1/(1+exp(-par(2)));  % RL 2  
% beta1  = exp(par(3));  % temp 1
% beta2  = exp(par(4));  % temp 2
% lambda = par(5);  % Elegibility trace discount  
% p = exp(par(6));       % perseveration
% w = 1/(1+exp(-par(7)));       % weight model/free  

% % If parameters should have the original values % %
alpha1 = par(1);
alpha2 = par(1);  % if 1 alpha: 1
beta1 = par(3);
beta2 = par(3);   % if 1 beta: 3
lambda = par(5);
p = par(6);                                                                 % choice perseverance
k = par(7);                                                                 % key perseverance
w = par(8);

%% Model-free Model (V contains the Values for model-free decision making)

% % Stage 2 value update: V2(t+1) = V2(t) + alpha2 * (r - V2(t))

clear V;
V = zeros(length(reward)+1,6);
V(1, 1:6) = 0.5;                                                            % initial values for each fractal

for i = 1:length(reward)
    delta2 = reward(i) - sum(V(i,:) .* choice2(i,:));                       % delta2 = r - V2(t); V2(t) = Value at chosen fractal (2nd stage)
    V(i+1,:) = V(i,:) + alpha2 * delta2 * choice2(i,:);                     % V(t+1) = V(t) + alpha2 * delta2; delta2 = delta at chosen fractal
end

% % Stage 1 value update: V1(t+1) = V1(t) + alpha1 * (V2(t) - V1(t) + lambda * alpha1 * (r - V1(t)))

v1 = V(:,1:2);                                                          % 1st-stage values (of both fractals)
v2 = sum(V(1:length(reward),:) .* choice2, 2);                              % 2nd-stage values (of chosen fractals only)

for i = 1:length(reward)
    v1_picked = sum(v1(i,:) .* choice1(i,:)) ;
    delta1 = v2(i) - v1_picked ;                    % delta1 = v2(t) - v1(t); v1(t) = value at chosen fractal (1st stage)
    theta1 = reward(i) - v1_picked ;                % theta1 = r - v1(t)
    v1(i+1,:)  = v1(i,:) + alpha1 * choice1(i,:) * (delta1 + lambda * alpha1 * theta1); % V1(t+1) = V1(t) + alpha1 * (delta1 + lambda * alpha1 * theta1)
end

V(:,1:2) = v1 ;
V(end, :) = [];                                                             % remove last row
% V contains values for each fractal in each trial; values are updated whenever a fractal is chosen

%% Model-based Model
% M (1 2 3 4) == (common transition to 3 & 4, common transition to 5 & 6, 2nd-stage value for 3 & 4, 2nd-stage value for 5 & 6)
M(:,3) = max(V(:, 3:4), [], 2);                                             % common part of fractal1 value = max value of fractal3 & fractal4
M(:,4) = max(V(:, 5:6), [], 2);                                             % common part of fractal2 value = max value of fractal5 & fractal6
M(:,1) = 0.7 * M(:, 3) + 0.3 * M(:, 4);                                     % combined fractal1 value = .7 * common part (3&4) + .3 * rare part (5&6)
M(:,2) = 0.3 * M(:, 3) + 0.7 * M(:, 4);                                     % combined fractal2 value = .7 * common part (5&6) + .3 * rare part (3&4)

%% Choice Repetition Perseveration
C = [0 0; choice1(1:end-1,:)];                                              % C = choice1 matrix with inital [0 0]
dchoice = [0 0; diff(choice1)];                                             % was a different fractal chosen in the next trial?
c_rep = (dchoice == 0) .* (choice1 == 1);                                   % was the same fractal chosen as in previous trial? separately for fractals
c_rep(:,3) = sum(c_rep, 2);                                                 % was the same fractal chosen? combined for fractals

%% Key Repetition Perseveration (IFF same key was pressed in both stages, does it increase the probability of pressing it again next time?)
key1_is_key2 = key1 .* key2;
K = [0 0; key1_is_key2(1:end-1,:)];
fract1_bonus = zeros(length(reward), 1);
fract2_bonus = zeros(length(reward), 1);
fract1_bonus((fract1_left == 1 & K(:,1) == 1) | ...   % When fract1 is left & left key was pressed last trial, fract1 gets a bonus
             (fract1_left == 0 & K(:,2) == 1)) = 1;   % or when fract1 is right & right key was pressed last time.
fract2_bonus((fract1_left == 0 & K(:,1) == 1) | ...   % When fract2 is left & left key was pressed last trial, fract2 gets a bonus
             (fract1_left == 1 & K(:,2) == 1)) = 1;   % or when fract2 is right & right was pressed.
% dkey = [0 0; diff(key1_is_key2)];
% k_rep = (dkey == 0) .* (key1_is_key2 == 1);
% k_rep(:,3) = sum(k_rep, 2);

%% Full Hybrid Value Model
% % Q(:,1:2) = w * model-based 1st-stage values + (1-w) * model-free 1st-stage values
Q = w * M(:, 1:2) + (1 - w) * V(:, 1:2);                                    
% % Q(:,3:6) = model-free 2nd-stage values
Q(:, 3:6) = V(:, 3:6);

%% Action Probability
% % Explanations % %
% Q(:,1)-Q(:,2) -> value-Preference for fractal1 over fractal2 (How much higher is fractal1's value (before reward) than fractal2's value)?
% Q(:,2)-Q(:,1) -> value-Preference for fractal2 over fractal1 (How much higher is fractal2's value (before reward) than fractal1's value)?
% C(:,1)-C(:,2) -> perseverance-preference for fractal1, because chosen in previous trial (Was fractal1 chosen in the previous trial -> result == 1; Was fractal2 chosen -> result == -1)
% C(:,2)-C(:,1) -> perseverance-preference for fractal2, because chosen in previous trial (Was fractal2 chosen in the previous trial -> result == 1; Was fractal1 chosen -> result == -1)
% fract1_bonus  -> change in probability of chosing fractal 1 because it sits on the key that that was pressed in the previous trial
% fract2_bonus  -> same for fractal 2

P(:,1) = 1 ./ ( 1 + exp(- (beta1 * (Q(:,1)-Q(:,2)) + p * (C(:,1)-C(:,2)) ...
    + k * (fract1_bonus-fract2_bonus) ))  );     % beta1 * value-preference for fractal1 + p * perseverance-preference for fractal1 % + k*(K(:,1)-K(:,2))
P(:,2) = 1 ./ ( 1 + exp(- (beta1 * (Q(:,2)-Q(:,1)) + p * (C(:,2)-C(:,1)) ...
    + k * (fract2_bonus-fract1_bonus) ))  );     % beta1 * value-preference for fractal2 + p * perseverance-preference for fractal2 % + k*(K(:,2)-K(:,1))
P(:,3) = exp(beta2*(Q(:,3))) ./ ( exp(beta2*(Q(:,3))) + exp(beta2*(Q(:,4))) );  
P(:,4) = exp(beta2*(Q(:,4))) ./ ( exp(beta2*(Q(:,3))) + exp(beta2*(Q(:,4))) );
P(:,5) = exp(beta2*(Q(:,5))) ./ ( exp(beta2*(Q(:,5))) + exp(beta2*(Q(:,6))) );
P(:,6) = exp(beta2*(Q(:,6))) ./ ( exp(beta2*(Q(:,5))) + exp(beta2*(Q(:,6))) );

% % Choice (t-1), values going into t, key (t), choice (t), and reward (t)
complete_matrix = [C Q key1 choice1 reward];

%% Log Likelihood & BIC
prob = P .* choice; prob(prob == 0) = nan;                                  % Model-Probabilities of chosing the action that was chosen
loglik1 = nansum(-log(prob(:,1:2)), 2);                                     % log likelihood stage 1 for each trial
loglik2 = nansum(-log(prob(:,3:6)), 2);                                     % log likelihood stage 2 for each trial
LogLik = sum(loglik1 + loglik2);                                            % Total log likelihood of the game (sum over stages & trials)
lik = LogLik / 2 / length(valid);                                           % average log likelihood per stage per trial
BIC = 2 * LogLik + num_params * log(2 * length(valid));                     % total BIC

CR = nan; VAL = nan;
if nargin > 2
    C = [V(:,2) > V(:,1) M(:,2) > M(:,1) Q(:,2) > Q(:,1) ch1-1];
    CR = C(:,1:3) == repmat(C(:,4),1,3);
    VAL = [V(:,1:2) M(:,1:2)];
end

end
