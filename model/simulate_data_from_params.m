function [simulated_data] = simulate_data_from_params(par, subject, session, number_of_trials, i)

% number_of_trials = 201;
% lower_param_boundaries = [.01 0   0  0  1 0  0 0];
% upper_param_boundaries = [.99 0  20  0  1 5 20 1];
% par = [.35 0  .25 0  0 .25 .1  0] .* upper_param_boundaries;         % use mean group values as start values
% par = 0.5 * ones(number_of_trials,8);

%% Get reward schedule of the current subject & session
reward_versions = csvread('C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\model\reward_versions.csv');
%reward_versions = csvread('C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\model\reward_versions.csv');
reward_version  = reward_versions((reward_versions(:,1) == subject & reward_versions(:,2) == session),3);
load(['C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\stimuli\list' num2str(reward_version) '.mat'])
%load(['C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\stimuli\list' num2str(reward_version) '.mat'])

task_probabilities = x(5:8,:);

%% Inititialize stuff
% % % Data frame for simulated data
simulated_data = zeros(number_of_trials, 15);
% % % Parameters
alpha1 = par(1);
alpha2 = par(1);
beta1  = par(3);
beta2  = par(3);
lambda = par(5);
p      = par(6);
k      = par(7);
w      = par(8);
% % % Reward matrix
reward = zeros(number_of_trials, 1);
% % % MF-values V and MB-values M for each fractal; combined model Q
V = zeros(length(reward) + 1, 6);
V(1,:) = 0.5;
M = zeros(length(reward) + 1, 4);
M(1,:) = 0.5;
Q = zeros(length(reward) + 1, 6);
Q(1,:) = 0.5;
% % % Choice perseverance C
C = zeros(length(reward), 2);
% % % Key1 perseverance
key1 = zeros(length(reward), 2);
key2 = zeros(length(reward), 2);
key1_is_key2 = zeros(length(reward), 2);
K = zeros(length(reward), 2);
K1_bonus = zeros(length(reward), 2);
K2_bonus = zeros(length(reward), 2);
% % % Choice probability matrix P
P = zeros(length(reward), 6);
% % % Choices (choice1 = 1st-stage choice; choice2 = 2nd-stage choice)
choice1 = zeros(length(reward), 6);
choice2 = zeros(length(reward), 6);
fract1_left  = zeros(length(reward), 1);
fract1_right = zeros(length(reward), 1);

for trial = 1:number_of_trials;
    
    % % % Where is which 1st-stage fractal presented?
    if rand < 0.5
        fract1a_fract1b = [1 2];   % fract A left and fract B right
        if K(trial,1) == 1         % two left keys in last trial
            K1_bonus(trial,:) = [1, -1];
        elseif K(trial,2) == 1     % two right keys in last trial
            K1_bonus(trial,:) = [-1, 1];
        end
    else
        fract1a_fract1b = [2 1];   % fract A right and fract B left
        if K(trial,1) == 1
            K1_bonus(trial,:) = [-1, 1];
        elseif K(trial,2) == 1
            K1_bonus(trial,:) = [1, -1];
        end
    end
    
    % % % Where is which 2nd-stage fractal presented?
    if rand < 0.5
        fract2a_fract2b = [1 2];   % fract A (3, 5) left and fract B (4, 6) right
        if K(trial,1) == 1         % two left keys in last trial
            K2_bonus(trial,:) = [1, -1];
        elseif K(trial,2) == 1     % two right keys in last trial
            K2_bonus(trial,:) = [-1, 1];
        end
    else
        fract2a_fract2b = [2 1];   % fract A right and fract B left
        if K(trial,1) == 1
            K2_bonus(trial,:) = [-1, 1];
        elseif K(trial,2) == 1
            K2_bonus(trial,:) = [1, -1];
        end
    end
    
    %% Calculate probabilities for each choice
    P(trial,1) = 1 ./ ( 1 + exp(- (beta1 * (Q(trial,1)-Q(trial,2)) + p * (C(trial,1)-C(trial,2)) + k * (K1_bonus(trial,1)-K1_bonus(trial,2))))  );
    P(trial,2) = 1 ./ ( 1 + exp(- (beta1 * (Q(trial,2)-Q(trial,1)) + p * (C(trial,2)-C(trial,1)) + k * (K1_bonus(trial,2)-K1_bonus(trial,1))))  );
    P(trial,3) = exp(beta2*(Q(trial,3))) ./ ( exp(beta2*(Q(trial,3))) + exp(beta2*(Q(trial,4))) );  
    P(trial,4) = exp(beta2*(Q(trial,4))) ./ ( exp(beta2*(Q(trial,3))) + exp(beta2*(Q(trial,4))) );
    P(trial,5) = exp(beta2*(Q(trial,5))) ./ ( exp(beta2*(Q(trial,5))) + exp(beta2*(Q(trial,6))) );
    P(trial,6) = exp(beta2*(Q(trial,6))) ./ ( exp(beta2*(Q(trial,5))) + exp(beta2*(Q(trial,6))) );

    %% Make choices in one trial
    if rand < P(trial, 1)
        % % % Chose fractal 1
        choice1(trial, 1) = 1;
        key1(trial, fract1a_fract1b(1)) = 1;
        if rand < 0.7
            % % % Common transition
            simulated_data(trial,15) = 0;   % transition
            simulated_data(trial, 6) = 1;   % 2nd-stage pair
            if rand < P(trial, 3)
                % % % Chose fractal 3
                choice2(trial, 3) = 1;
                key2(trial, fract2a_fract2b(1)) = 1;
                if rand < task_probabilities(1, trial)
                    reward(trial) = 1;
                end
            else
                % % % Chose fractal 4
                choice2(trial, 4) = 1;
                key2(trial, fract2a_fract2b(2)) = 1;
                if rand < task_probabilities(2, trial)
                    reward(trial) = 1;
                end
            end
        else
            % % % Rare transition
            simulated_data(trial,15) = 1;
            simulated_data(trial, 6) = 2;
            if rand < P(trial, 5)
                % % % Chose fractal 5
                choice2(trial, 5) = 1;
                key2(trial, fract2a_fract2b(1)) = 1;
                if rand < task_probabilities(3, trial)
                    reward(trial) = 1;
                end
            else
                % % % Chose fractal 6
                choice2(trial, 6) = 1;
                key2(trial, fract2a_fract2b(2)) = 1;
                if rand < task_probabilities(4, trial)
                    reward(trial) = 1;
                end
            end
        end
    else
        % % % Chose fractal 2
        choice1(trial, 2) = 1;
        key1(trial, fract1a_fract1b(2)) = 1;
        if rand < 0.7
            % % % Common transition
            simulated_data(trial,15) = 0;
            simulated_data(trial, 6) = 2;
            if rand < P(trial, 5)
                % % % Chose fractal 5
                choice2(trial, 5) = 1;
                key2(trial, fract2a_fract2b(1)) = 1;
                if rand < task_probabilities(3, trial)
                    reward(trial) = 1;
                end
            else
                % % % Chose fractal 6
                choice2(trial, 6) = 1;
                key2(trial, fract2a_fract2b(2)) = 1;
                if rand < task_probabilities(4, trial)
                    reward(trial) = 1;
                end
            end
        else
            % % % Rare transition
            simulated_data(trial,15) = 1;
            simulated_data(trial, 6) = 1;
            if rand < P(trial, 3)
                % % % Chose fractal 3
                choice2(trial, 3) = 1;
                key2(trial, fract2a_fract2b(1)) = 1;
                if rand < task_probabilities(1, trial)
                    reward(trial) = 1;
                end
            else
                % % % Chose fractal 4
                choice2(trial, 4) = 1;
                key2(trial, fract2a_fract2b(2)) = 1;
                if rand < task_probabilities(2, trial)
                    reward(trial) = 1;
                end
            end
        end
    end

    %% Update values based on trial outcome

    % % % Model-free Model V
    % Second stage
    delta2 = reward(trial) - sum(V(trial,:) .* choice2(trial,:));
    V(trial+1,:) = V(trial,:) + alpha2 * delta2 * choice2(trial,:);  
    % First stage
    V_1st_stage = sum(V(trial,:) .* choice1(trial,:), 2);
    V_2nd_stage = sum(V(trial,:) .* choice2(trial,:), 2);
    delta1 = V_2nd_stage - V_1st_stage;
    theta1 = reward(trial) - V_1st_stage;
    V(trial+1, 1:2) = V(trial, 1:2) + alpha1 * choice1(trial, 1:2) * (delta1 + lambda * alpha1 * theta1);

    % % % Model-based Model M
    M(trial+1,3) = max(V(trial+1, 3:4), [], 2);
    M(trial+1,4) = max(V(trial+1, 5:6), [], 2);
    M(trial+1,1) = 0.7 * M(trial+1, 3) + 0.3 * M(trial+1, 4);
    M(trial+1,2) = 0.3 * M(trial+1, 3) + 0.7 * M(trial+1, 4);     

    % % % Full hybrid model Q
    Q(trial+1, 1:2) = w * M(trial+1, 1:2) + (1 - w) * V(trial+1, 1:2);
    Q(trial+1, 3:6) = V(trial+1, 3:6);

    % % % Choice perseverance
    C(trial+1,:) = choice1(trial, 1:2);
    
    % % % Key perseverance
    key1_is_key2(trial,:) = key1(trial,:) .* key2(trial);
    K(trial+1,:) = key1_is_key2(trial,:);
end

%% Fill up simulated data
%  1. trial number
%  2. first stage keypress
%  3. second stage keypress
%  4. first stage stimulus chosen
%  5. second stage stimulus chosen
%  6. second stage pair shown
%  7. rt first stage
%  8. rt second stage
%  9. ITI going into this trial
% 10. reward received
% 11. 1st phase, stimulus left
% 12. 1st phase, stimulus right
% 13. 2nd phase, stimulus left
% 14. 2nd phase, stimulus right
% 15. common (0) or uncommon (1) transition
% 16. SubjID
% 17. session
% 18. simulation #

simulated_data(:, 1) = 1:number_of_trials;
simulated_data(:, 2) = sum(repmat(1:2, number_of_trials, 1) .* key1, 2);
simulated_data(:, 3) = sum(repmat(1:2, number_of_trials, 1) .* key2, 2);
simulated_data(:, 4) = sum(repmat(1:2, number_of_trials, 1) .* choice1(:,1:2), 2);
simulated_data(:, 5) = sum(repmat(3:6, number_of_trials, 1) .* choice2(:,3:6), 2);
simulated_data(:,10) = reward;
simulated_data(:,16) = subject * ones(length(reward), 1);
simulated_data(:,17) = session * ones(length(reward), 1);
simulated_data(:,18) = i * ones(length(reward), 1);
