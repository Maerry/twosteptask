number_of_simulations = 20;

subj_params = csvread('..\Results\all\parameter_data_ga_fmincon_abpkw.csv');

subjects = unique(subj_params(subj_params(:,10) ~= 33, 10));
sessions = unique(subj_params(:,11));


for subject = subjects'
    for session = sessions'
        simulations = zeros(201 * number_of_simulations, 18);
        row = 1;
        for i = 1:number_of_simulations
            par = subj_params(subj_params(:,10) == subject & subj_params(:,11) == session,:);
            simulations(row:(row+200),:) = simulate_data_from_params(par, subject, session, 201, i);
            row = row + 201;
        end
        csvwrite(['..\Results\simulated_data\simulations' num2str(subject) '_' num2str(session) '.csv'], simulations)
    end
end
