function estimate_parameters_from_mat (num_params, lower_param_boundaries, upper_param_boundaries, param_start_values, solver, suffix)

%% Estimate parameters

% Set directions to folders containing experiment data (file_dir) and scripts (script_dir)
file_dir = 'C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\Results';
% file_dir = 'C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\Results';
script_dir = 'C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TwoStepTask\model';
%script_dir = 'C:\Users\lclusr\Documents\MATLAB\trainingplanningproject\TwoStepTask\model';
cd(script_dir)
addpath(genpath(script_dir))

%% Columns of data_import
% 
% 1.      params.user.log (colum10 = reward; column15 = rare transition (vs. common); column16 = stay (vs. switch); colum17 = switch; column18 = test (vs. train))
% 2.      parameters by get_log_likelihood_of_params
% 3.-4.   fmincom parameters
% 5.-11.  fmincon parameters - spread out (alpha1, alpha2, beta1, beta2, lamda, p, k, w)
% 12.     SubjID
% 13.     Session
% 14.     Run

data_import = cell(32,1);
par = [.5 .5  .5 .5  .5 .5 .5  .5];
% num_params = 4;
% lower_param_boundaries = [ 0 0   0  0  1 -5  0  .01];
% upper_param_boundaries = [ 1 0  20  0  1  5  0  .99];
% param_start_values =     [.3 0  .3  0  1 .2 .3  .40] .* upper_param_boundaries;         % use mean group values as start values
% solver = 'ga_and_fmincon';                                                  % options: fmincon; patternsearch; particleswarm; ga; simulannealbnd; ga_and_fmincon; BEST: ga_and_fmincon; FASTEST: ga?
% particleswarm, ga, and ga_and_fmincon produce similar results (fmincon and patternsearch alone don't get k)

%% Calculate optimal parameters for each data set
files = dir([file_dir '\rawdata2015\']);
fileIndex = find(~[files.isdir]);

for k = 1:length(fileIndex)

    % % find and load each file in the directory % %
    fileName = files(fileIndex(k)).name;
    load([file_dir '\rawdata2015\' fileName]);                                  % load each data file in the folder
    trial = params.user.log;                                                % save data file into 'trial'

    % % get run (1 or 2) % %
    if length(trial) == 50                                                  % find out if the data are from training (0) or testing run (1)
        run = 1;
    else
        run = 2;
    end

    % % get switch/stay columns % %
    [stim1, key1, rep_stim1, rep_key1] = swstay_M(trial);                   % the 2 columns correspond to the 2 stimuli of the 1st stage; stay indicates if the 1st stimulus was repeatedly chosen (column 1) or the 2nd one (column 2) or either (column 3)

    % % exclude invalid trials (no response) % %
    valid = trial(:,2) ~= 0 & trial(:,3) ~= 0;                              % find all trials where responses were given in 1st AND 2nd stage
    trial = trial(valid,:);                                                 % exclude all other trials

    % % get SubjID and session % %
    SubjID  = str2double(params.user.number);
    session = params.user.session;
    
    % % save everything into data_import % %
    data_import{k,1} = [trial, stim1, rep_stim1, key1, rep_key1, SubjID * ones(length(trial),1), session * ones(length(trial),1), run * ones(length(trial),1)];
    
    % % estimate parameters and save into data_import as well % %
    [ reward, choice1, choice2, key1, key2, k1, valid, ch1 ] = GetTrialData( trial );
    fract1_left = trial(:,11);
    
% According to http://de.mathworks.com/help/gads/choosing-a-solver.html,
% the best solver for GLOBAL minima (nonsmooth or smooth objective and
% constraints) is patternsearch ("Proven convergence to local optimum,
% slower than gradient-based solvers")

% Function descriptions:
    % [x,fval] =        fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon)
    % [x,fval] =  patternsearch(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
    % [x,fval] =  particleswarm(fun,nvars,lb,ub,options)
    % [x,fval] = ga(fitnessfcn,nvars,A,b,Aeq,beq,LB,UB,nonlcon,options)
    % [x,fval] = simulannealbnd(fun,x0,lb,ub,options)
    
    if strcmp(solver, 'fmincon')
        [data_import{k,3}, data_import{k,4}] = fmincon(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...    % save optimal parameters in {k, 3} and function values in {k, 4}
                                                    param_start_values, [],[],[],[], ...
                                                    lower_param_boundaries, upper_param_boundaries);
    elseif strcmp(solver, 'patternsearch')
%         opts = psoptimset('PlotFcns',{@psplotbestf,@psplotfuncount});     % For plotting patternsearch progress
%         opts = psoptimset('TolX', 1e-17, 'TolFun', 1e-17);                % To make search more accurate
        [data_import{k,3}, data_import{k,4}] = patternsearch(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...    % save optimal parameters in {k, 3} and function values in {k, 4}
                                                    param_start_values, [],[],[],[], ...
                                                    lower_param_boundaries, upper_param_boundaries, [], opts);
    elseif strcmp(solver, 'particleswarm')
        [data_import{k,3}, data_import{k,4}] = particleswarm(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...    % save optimal parameters in {k, 3} and function values in {k, 4}
                                                    length(par), ...
                                                    lower_param_boundaries, upper_param_boundaries);
    elseif strcmp(solver, 'ga')
        [data_import{k,3}, data_import{k,4}] = ga(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...    % save optimal parameters in {k, 3} and function values in {k, 4}
                                                    length(par), [],[],[],[], ...
                                                    lower_param_boundaries, upper_param_boundaries, [],...
                                                    gaoptimset('PopulationSize', 500));
    elseif strcmp(solver, 'simulannealbnd')
        [data_import{k,3}, data_import{k,4}] = simulannealbnd(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...  
                                                    param_start_values, ...
                                                    lower_param_boundaries, upper_param_boundaries);
    elseif strcmp(solver, 'ga_and_fmincon')
        estimated_parameters = ga(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...    % save optimal parameters in {k, 3} and function values in {k, 4}
                                                    length(par), [],[],[],[], ...
                                                    lower_param_boundaries, upper_param_boundaries, [],...
                                                    gaoptimset('PopulationSize', 1000));
        [data_import{k,3}, data_import{k,4}] = fmincon(@(par)get_log_likelihood_of_params(num_params, par, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left), ...    % save optimal parameters in {k, 3} and function values in {k, 4}
                                                    estimated_parameters, [],[],[],[], ...
                                                    lower_param_boundaries, upper_param_boundaries);
    end

      [~,~,~,~, data_import{k,2}] = get_log_likelihood_of_params(num_params, data_import{k,3}, reward, choice1, choice2, key1, key2, k1, valid, ch1, fract1_left);    % BIC of the model & estimated parameter values

    % % Add SubjID, session, and run % %    
    data_import{k,12} = SubjID;
    data_import{k,13} = session;
    data_import{k,14} = run;
    
    % % convert the cell data to matrix format % %
    data_import{k,5} = data_import{k,3}(1); % alpha1
    data_import{k,6} = data_import{k,3}(2); % alpha2
    data_import{k,7} = data_import{k,3}(3); % beta1
    data_import{k,8} = data_import{k,3}(4); % beta2
    data_import{k,9} = data_import{k,3}(5); % lambda
    data_import{k,10} = data_import{k,3}(6); % p
    data_import{k,11} = data_import{k,3}(7); % k
    data_import{k,15} = data_import{k,3}(8); % w
    data_import{k,16} = data_import{k,4}; % function value (hopefully the minimum)
    
%     csvwrite([file_dir '\all\k_and_p_can_be_negative\parameter_data_ga_fmincon' num2str(SubjID) num2str(session) num2str(run) '.csv'], ...
%         [data_import{k,[5:11 15 2 12:14 16]}]);
end

parameter_dat = cell2mat(data_import(:,[5:11 15 2 12:14 16]));                 
% alpha1, alpha2, beta1, beta2, lambda, p, k, w; BIC; subjID, session, run, function value

% save([script_dir '\maria_data_import_cell.mat'], 'data_import');
csvwrite([file_dir '\all\k_and_p_can_be_negative\parameter_data_ga_fmincon_' suffix '.csv'], parameter_dat);
