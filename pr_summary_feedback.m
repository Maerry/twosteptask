function pr_summary_feedback(trial)

global exp
global params

%% Determine number of points won
%%% If there was no summary feedback yet
if ~any(exp.feedback_trials(1:(trial-1)) == 5)
    % count rewards from the beginning of the task
    trial_with_last_feedback = 0;
    
%%% If there already was a summary feedback
else
    % count the rewards since then
    trial_with_last_feedback = find(exp.feedback_trials(1:(trial-1)) == 5, 1, 'last');
end

%%% Count the rewards
trials_correct_since_last_feedback = nansum(params.user.log((trial_with_last_feedback+1):trial,10));

%% Write and display the message
m1 = ['You earned ', num2str(trials_correct_since_last_feedback), ' coins in the last ', num2str(exp.summary_feedback_trial.two_step), ' trials!'];
m2 = 'Press space to resume the task.';

config_keyboard;
cgpencol(1, 1, 1);
cgfont('Arial', 40);
cgflip(0, 0, 0);
cgtext(m1, 0,  200); 
cgtext(m2, 0, -200); 
cgflip(0, 0, 0);
wait(1000);

ks = zeros(255, 1);
cgkeymap; %clear keymap
while ~(any(ks))   % wait for any keypress
   [ks] = cgkeymap;
end

