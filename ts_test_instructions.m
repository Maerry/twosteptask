function ts_test_instructions

global params

cgfont(params.text.font,params.text.font_size);
cgpencol(params.text.color);

msg1 = 'That was the practice phase';
msg2 = 'If you have any questions, feel free to ask the experimenter';
msg3 = 'From now on you''ll start making money whenever you see the pound';
msg4 = 'Play well and earn yourself a big payout!';
msg5 = 'Press any key to continue';

cgmakesprite(102,params.resolution(1),params.resolution(2),params.background);
cgsetsprite(102);
cgtext(msg1,0,params.resolution(1)/6);
cgtext(msg2,0,0);
cgtext(msg3,0,-params.resolution(1)/12);
cgtext(msg4,0,-params.resolution(1)/10);
cgtext(msg5,0,-params.resolution(1)/8);

ts_wait_input(102,0,0);