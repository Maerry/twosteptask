function ts_instruction_flag = ts_instruction_quiz %progress: slides work, questions don't

global exp
global params

%% Quiz text (English only)
if strcmp(exp.data.language, 'English')
    m01 = 'For the quiz, you will be shown several questions and 4 answer choices for each question.';
    m02 = 'Please click 1, 2, 3, or 4 (on the number row, not the numpad), corresponding to the correct answer.';
    m03 = 'Nice job!';
    m04 = 'Sorry, your answer was incorrect.';
    m05 = 'Press any key to continue.';
    m10 = 'Which of the following is true?';
    m11 = '1) If you click the square, you will usually go to the circle and triangle.';
    m12 = '2) The cross and heart will sometimes appear together.';
    m13 = '3) If the heart gives you a reward once, it will always give you a reward.';
    m14 = '4) You will always see the pentagon and square first.';  
    m20 = 'Which of the following pairs is not possible?';
    m21 = '1) Circle and triangle.';
    m22 = '2) Cross and heart.';
    m23 = '3) Triangle and heart.';
    m24 = '4) Pentagon and square.';  
    m30 = 'Which pair is more likely to show up if you click the square?';
    m31 = '1) Circle and triangle.';
    m32 = '2) Cross and heart.';
    m33 = '3) Equally likely.';
    m34 = '4) Neither pair.';   % set cutoff at 4/5: add in question re: reward probabilitieis changing or constant over time: true/false; also note that only last image alone determines  
    m40 = 'Which of the following pictures determines the probability of receiving a reward?';
    m41 = '1) The first picture you select.';
    m42 = '2) The second picture you select.';
    m43 = '3) The combination of the two pictures you select.';
    m44 = '4) None of the above.';
    m50 = 'True or false: The reward probability for a given picture stays constant throughout the experiment.';
    m51 = '1) True.';
    m52 = '2) False.';
    m60 = 'Congratulations! You passed the quiz.';
    m61 = 'Do you have any other questions about this task?';
    m62 = 'Press any key when you are ready to start the task!';
    m70 = 'Unfortunately, you did not pass the quiz.';
    m71 = 'For your reference, here are the instructions again.';
else
    m01 = 'placeholder';
end

cgfont(params.text.font, params.text.font_size);
cgpencol(params.text.color);
ts_instruction_flag = 0; %reset flag
delaytime = 500; %amount of time to wait before subject can answer the question
responsetime = 60000; %amount of time subject has to answer the question

%% Quiz instructions slide
upper_end = 200;
cgmakesprite(695, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(695);

cgtext(m01, 0, upper_end);
cgtext(m02, 0, upper_end-100);

ts_wait_input(695, 0, 0);

%% 1st question - order/appearance of pictures
cgmakesprite(700, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(700);

cgtext(m10, 0, upper_end);
cgtext(m11, 0, upper_end-100);
cgtext(m12, 0, upper_end-120);
cgtext(m13, 0, upper_end-140);
cgtext(m14, 0, upper_end-160);
cgloadbmp(701, 'stimuli/E1.bmp', 200, 200);
cgdrawsprite(701, 0, upper_end-300);

cgsetsprite(0);
cgdrawsprite(700, 0, 0);
cgflip(params.background);
wait(delaytime);
cgkeymap;
t = time;           %start timer
ks = zeros(255, 1);
too_late = 0;

while ~(ks(2)) && ~(ks(3)) && ~(ks(4)) && ~(ks(5)) %wait for keypress (1 to 4)
   [ks] = cgkeymap;
   
   if time - t > responsetime % response deadline - it's in ts_initialize
       too_late = 1;
       break;
    
   end
end  
cgflip(params.background);
   
if too_late == 0 %if subject was on time
    ks([1 6:255]) = 0;
    key = find(ks,1);
elseif too_late == 1 %if subject was too late
    key = 0;
end

cgflip(params.background);

if key == 5
     cgmakesprite(725, params.resolution(1), params.resolution(2), params.background);
     cgsetsprite(725);
     cgtext(m03, 0, upper_end);                                          % give feedback 'Nice job!'
     cgtext(m05, 0, upper_end-100); 
     ts_wait_input(725, 0, 0);
else
    cgmakesprite(730, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(730);
    cgtext(m04, 0, upper_end);                                          % give feedback 'Sorry, your answer was incorrect.'
    cgtext(m05, 0, upper_end-100); 
    ts_instruction_flag = ts_instruction_flag + 1;
    ts_wait_input(730, 0, 0);
end

%% 2nd question - possible pairings
cgmakesprite(705, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(705);

cgtext(m20, 0, upper_end);
cgtext(m21, 0, upper_end-100);
cgtext(m22, 0, upper_end-120);
cgtext(m23, 0, upper_end-140);
cgtext(m24, 0, upper_end-160);
cgloadbmp(706, 'stimuli/E1.bmp', 200, 200);
cgdrawsprite(706, 0, upper_end-300);

cgsetsprite(0);
cgdrawsprite(705, 0, 0);
cgflip(params.background);
wait(delaytime);
cgkeymap;
t = time;           %start timer
ks = zeros(255, 1);
too_late = 0;

while ~(ks(2)) && ~(ks(3)) && ~(ks(4)) && ~(ks(5)) %wait for keypress
   [ks] = cgkeymap;
   
   if time - t > responsetime
       too_late = 1;
       break;
    
   end
end  
cgflip(params.background);
   
if too_late == 0 %if subject was on time
    ks([1 6:255]) = 0;
    key = find(ks,1);
elseif too_late == 1 %if subject was too late
    key = 0;
end

cgflip(params.background);

if key == 4
     cgmakesprite(725, params.resolution(1), params.resolution(2), params.background);
     cgsetsprite(725);
     cgtext(m03, 0, upper_end);                                          % give feedback 'Nice job!'
     cgtext(m05, 0, upper_end-100); 
     ts_wait_input(725, 0, 0);
else
    cgmakesprite(730, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(730);
    cgtext(m04, 0, upper_end);                                          % give feedback 'Sorry, your answer was incorrect.'
    cgtext(m05, 0, upper_end-100); 
    ts_instruction_flag = ts_instruction_flag + 1;
    ts_wait_input(730, 0, 0);
end

%% 3rd question - which pair is more likely
cgmakesprite(710, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(710);

cgtext(m30, 0, upper_end);
cgtext(m31, 0, upper_end-100);
cgtext(m32, 0, upper_end-120);
cgtext(m33, 0, upper_end-140);
cgtext(m34, 0, upper_end-160);
cgloadbmp(711, 'stimuli/E2.bmp', 200, 200);
cgdrawsprite(711, 0, upper_end-300);

cgsetsprite(0);
cgdrawsprite(710, 0, 0);
cgflip(params.background);
wait(delaytime);
cgkeymap;
t = time;           %start timer
ks = zeros(255, 1);
too_late = 0;

while ~(ks(2)) && ~(ks(3)) && ~(ks(4)) && ~(ks(5)) %wait for keypress
   [ks] = cgkeymap;
   
   if time - t > responsetime
       too_late = 1;
       break;
    
   end
end  
cgflip(params.background);
   
if too_late == 0 %if subject was on time
    ks([1 6:255]) = 0;
    key = find(ks,1);
elseif too_late == 1 %if subject was too late
    key = 0;
end

cgflip(params.background);

if key == 3
     cgmakesprite(725, params.resolution(1), params.resolution(2), params.background);
     cgsetsprite(725);
     cgtext(m03, 0, upper_end);                                          % give feedback 'Nice job!'
     cgtext(m05, 0, upper_end-100); 
     ts_wait_input(725, 0, 0);
else
    cgmakesprite(730, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(730);
    cgtext(m04, 0, upper_end);                                          % give feedback 'Sorry, your answer was incorrect.'
    cgtext(m05, 0, upper_end-100); 
    ts_instruction_flag = ts_instruction_flag + 1;
    ts_wait_input(730, 0, 0);
end

%% 4th question - which picture determines the reward probability
cgmakesprite(735, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(735);

cgtext(m40, 0, upper_end);
cgtext(m41, 0, upper_end-100);
cgtext(m42, 0, upper_end-120);
cgtext(m43, 0, upper_end-140);
cgtext(m44, 0, upper_end-160);
cgloadbmp(736, 'stimuli/E2.bmp', 200, 200);
cgdrawsprite(736, 0, upper_end-300);

cgsetsprite(0);
cgdrawsprite(735, 0, 0);
cgflip(params.background);
wait(delaytime);
cgkeymap;
t = time;           %start timer
ks = zeros(255, 1);
too_late = 0;

while ~(ks(2)) && ~(ks(3)) && ~(ks(4)) && ~(ks(5)) %wait for keypress
   [ks] = cgkeymap;
   
   if time - t > responsetime
       too_late = 1;
       break;
    
   end
end  
cgflip(params.background);
   
if too_late == 0 %if subject was on time
    ks([1 6:255]) = 0;
    key = find(ks,1);
elseif too_late == 1 %if subject was too late
    key = 0;
end

cgflip(params.background);

if key == 3
     cgmakesprite(725, params.resolution(1), params.resolution(2), params.background);
     cgsetsprite(725);
     cgtext(m03, 0, upper_end);                                          % give feedback 'Nice job!'
     cgtext(m05, 0, upper_end-100); 
     ts_wait_input(725, 0, 0);
else
    cgmakesprite(730, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(730);
    cgtext(m04, 0, upper_end);                                          % give feedback 'Sorry, your answer was incorrect.'
    cgtext(m05, 0, upper_end-100); 
    ts_instruction_flag = ts_instruction_flag + 1;
    ts_wait_input(730, 0, 0);
end

%% 5th question - do reward probabilities change over time 
cgmakesprite(740, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(740);

cgtext(m50, 0, upper_end);
cgtext(m51, 0, upper_end-100);
cgtext(m52, 0, upper_end-120);

cgsetsprite(0);
cgdrawsprite(740, 0, 0);
cgflip(params.background);
wait(delaytime);
cgkeymap;
t = time;           %start timer
ks = zeros(255, 1);
too_late = 0;

while ~(ks(2)) && ~(ks(3)) %wait for keypress
   [ks] = cgkeymap;
   
   if time - t > responsetime
       too_late = 1;
       break;
    
   end
end  
cgflip(params.background);
   
if too_late == 0 %if subject was on time
    ks([1 4:255]) = 0;
    key = find(ks,1);
elseif too_late == 1 %if subject was too late
    key = 0;
end

cgflip(params.background);

if key == 3
     cgmakesprite(725, params.resolution(1), params.resolution(2), params.background);
     cgsetsprite(725);
     cgtext(m03, 0, upper_end);                                          % give feedback 'Nice job!'
     cgtext(m05, 0, upper_end-100); 
     ts_wait_input(725, 0, 0);
else
    cgmakesprite(730, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(730);
    cgtext(m04, 0, upper_end);                                          % give feedback 'Sorry, your answer was incorrect.'
    cgtext(m05, 0, upper_end-100); 
    ts_instruction_flag = ts_instruction_flag + 1;
    ts_wait_input(730, 0, 0);
end

%% checking if they were able to answer at least 4/5 questions correctly.
if ts_instruction_flag < 1.5
    cgmakesprite(715, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(715);

    cgtext(m60, 0, upper_end);
    cgtext(m61, 0, upper_end-100);
    cgtext(m62, 0, upper_end-120);

    ts_wait_input(715, 0, 0);
else
    cgmakesprite(720, params.resolution(1), params.resolution(2), params.background);
    cgsetsprite(720);

    cgtext(m70, 0, upper_end);
    cgtext(m71, 0, upper_end-100);

    ts_wait_input(720, 0, 0);
end