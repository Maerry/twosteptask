# README #

This README documents whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains the code and stimulus files for the 2-step task.
* The 2-step task is the test task ("transfer task") in the TrainingPlanning experiment. It allows to determine what amount of model-based and model-free strategies test subjects use.

### Who do I talk to? ###

* Maria Eckstein (maria.eckstein@berkeley.edu)