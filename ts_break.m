function ts_break

global params;
global exp;

if strcmp(exp.data.language, 'English')
    m1 = 'You have a 30 second break now.';
    m2 = 'Ready? Press any key to start.';
else
    m1 = 'Du hast jetzt eine 60 Sekunden Pause.';
    m2 = 'Bereit? Dr�cke eine Taste, um weiterzumachen.';
end

cgfont(params.text.font,params.text.font_size);
cgpencol(params.text.color);

cgtext(m1, 0, 0);
cgflip(params.background);
wait(params.task.break.duration*1000);

cgmakesprite(501,params.resolution(1),params.resolution(2));
cgsetsprite(501);
cgtext(m2, 0, 0);
cgsetsprite(0);
ts_wait_input(501,0,0);
cgflip(params.background);
wait(1000);