function ts_instruction_reminder

global params
global exp

%% Write messages
if strcmp(exp.version, 'A')
    m11 = 'Great job!';
    m12 = 'You will now get a different set of pictures - continue collecting coins!';
    m13 = '(Press space to move on.)';
else
    m11 = 'You will now play the task again, in which you chose pictures and collect coins.';
    m12 = 'You will have a new set of pictures, with new probabilities of giving a reward.';
    m12b= 'Select the pictures with the left and right arrow keys.';
    m13 = '(Press space to move on.)';
end

%% Present messages
cgfont(params.text.font, params.text.font_size);
cgpencol(params.text.color);
upper_end = 200;
cgmakesprite(700, params.resolution(1), params.resolution(2), params.background);
cgsetsprite(700);

cgtext(m11, 0, upper_end);
cgtext(m12, 0, upper_end-100);
if ~strcmp(exp.version, 'A')
    cgtext(m12b, 0, upper_end-150);
end
cgtext(m13, 0,-upper_end);
ts_wait_input(700, 0, 0);
