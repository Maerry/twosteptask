for i = 1:1000
x = [];
params.task.lower_boundary = 0.2
params.task.upper_boundary = 0.8

for i = 1:4
x(i,1) = (randi(params.task.upper_boundary*100 - params.task.lower_boundary*100 + 1,1) + params.task.lower_boundary*100 - 1) / 100;
end

for i = 2:201
    for j = 1:4
        x(j,i) = x(j,i-1) + 0.025*randn;
        if x(j,i) > params.task.upper_boundary x(j,i) = params.task.upper_boundary; elseif x(j,i) < params.task.lower_boundary x(j,i) = params.task.lower_boundary; end
    end
end

plot(1:201,x(1:4,:))
axis([0 201 0 1])
drawnow
pause
end